@ECHO OFF

REM ####################################################################
REM #                                                                  #
REM #                       T T F I N S T A L L                        #
REM #                                                                  #
REM #            GUI for the 'embed' application of 'Tom 7'            #
REM #       Changes the embedding level of true type fonts (TTF)       #
REM #                                                                  #
REM #            Copyright 2019 by PB-Soft / Patrick Biegel            #
REM #                                                                  #
REM #                       https://pb-soft.com                        #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #               TTFinstall version: 1.0 / 20.06.2019               #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #     Repository: https://bitbucket.org/Blackneron/TTFinstall/     #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #  C H A N G E S :                                                 #
REM #                                                                  #
REM #  Version 1.0                                                     #
REM #                                                                  #
REM #  - Initial version of TTFinstall                                 #
REM #                                                                  #
REM ####################################################################

REM Clear the screen.
CLS

REM Display the title.
ECHO ###################################################################
ECHO #                                                                 #
ECHO #   TTFinstall - Changes the embedding level of true type fonts   #
ECHO #                                                                 #
ECHO ###################################################################
ECHO #                                                                 #
ECHO #  Usage:                                                         #
ECHO #                                                                 #
ECHO #  1. Put your true type font files into the directory 'input'.   #
ECHO #  2. Run the script to set the embedding level to 'installable'. #
ECHO #  3. Get the adjusted font files from the directory 'output'.    #
ECHO #                                                                 #
ECHO ###################################################################
ECHO.
PAUSE

REM Clear the screen.
CLS

REM Create an input directory if it does not exist.
ECHO.
ECHO Check/create the input directory...
ECHO.
IF NOT EXIST input MKDIR input

REM Create an output directory if it does not exist.
ECHO Check/create the output directory...
ECHO.
IF NOT EXIST output MKDIR output

REM First copy all the fonts to the output directory.
ECHO Copy original font files to the output directory...
copy "input\*.ttf" "output"
ECHO.

REM Change the embedding level to 'installable'.
ECHO Making the font files 'installable'...

REM Loop through the TTF files in the output directory.
FOR /R %%F IN (output/*.ttf) DO (

    REM Check if the original file exists, then convert the font.
    ECHO %%~nxF
    IF EXIST "input/%%~nxF" embed.exe "output/%%~nxF"
)

REM Display an information message.
ECHO.
ECHO The TTF fonts in the output directory are now installable!
ECHO.
PAUSE
