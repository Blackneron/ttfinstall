# TTFinstall Script - README #
---

### Overview ###

The **TTFinstall** tool is a simple batch script and a GUI for the [**Embed Application**](http://carnage-melon.tom7.org/embed/) command line tool from [**Tom 7**](http://carnage-melon.tom7.org/). The script will use the Embed executable (64 Bit) to change the embedding level of true type fonts (TTF) to 'installable' and therefore fixes the well known issue with the Internet Explorer's OpenType embedding permissions checks which generates the following error:

**CSS3114 @font-face failed OpenType embedding permission check. Permission must be Installable.**

For more information about this error please check the [**Microsoft Documentation**](https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/samples/hh180764(v=vs.85)).

To change the embedding level of the original (not installable) true type font files, they have to be copied to the subdirectory 'input' and after running the script the adjusted files are available in the subdirectory 'output'.

The [**Embed Application**](http://carnage-melon.tom7.org/embed/) is also available as an [**Online Version**](https://www.andrebacklund.com/fontfixer.html) from [**Andre Backlund**](https://www.andrebacklund.com).

### Screenshots ###

![TTFinstall - Font properties before changes](development/readme/ttfinstall_1.jpg "TTFinstall - Font properties before changes")

![TTFinstall - Start screen with instructions](development/readme/ttfinstall_2.jpg "TTFinstall - Start screen with instructions")

![TTFinstall - Script output of the font changes](development/readme/ttfinstall_3.jpg "TTFinstall - Script output of the font changes")

![TTFinstall - Font properties after changes](development/readme/ttfinstall_4.jpg "TTFinstall - Font properties after changes")

### Setup ###

* Copy the directory **ttfinstall** to your Windows computer.
* The directory contains the **embed.exe** executable, the **ttfinstall.bat** batch file and the license folder.
* The Embed application is a 64 Bit version so please make sure your computer can run it.
* You can get a 16 bit version from the [**Embed Website**](http://carnage-melon.tom7.org/embed/).
* First copy all your true type fonts to the subdirectory **ttfinstall\input**.
* Doubleclick the script file **ttfinstall.bat** to start the adjustment process.
* On the start screen you have to press 'any key' to proceed.
* The fonts will be adjusted and the result is displayed on the screen.
* Now you can get the 'installable' font files from the subdirectory **ttfinstall\output**.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **TTFinstall** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
